import disposableEnv from "../src";

// While typescript is happy to convert the "using" keyword into something
// functional, but does not want to do any actual polyfill-ing.

// In general, it's expected that you might use something like core-js or
// babel to provide polyfills. In this case, however, that's over-kill:
// all we need here is Symbol.dispose, and we can get it by just doing this:
// @ts-expect-error
Symbol.dispose ??= Symbol("Symbol.dispose");
// See https://devblogs.microsoft.com/typescript/announcing-typescript-5-2-beta/

describe("disposableEnv", () => {
  it("sets un-set variables", () => {
    process.env = {};
    (() => {
      using _ = disposableEnv({ KEY: "VALUE" });
      expect(process.env.KEY).toBe("VALUE");
    })();
    expect(process.env.KEY).toBe(undefined);
  });

  it("un-sets set variables", () => {
    process.env = { KEY: "VALUE" };
    (() => {
      using _ = disposableEnv({ KEY: undefined });
      expect(process.env.KEY).toBe(undefined);
    })();
    expect(process.env.KEY).toBe("VALUE");
  });

  it("overrides set set variables", () => {
    process.env = { KEY: "VALUE1" };
    (() => {
      using _ = disposableEnv({ KEY: "VALUE2" });
      expect(process.env.KEY).toBe("VALUE2");
    })();
    expect(process.env.KEY).toBe("VALUE1");
  });

  it("leaves other variables alone", () => {
    process.env = { KEY: "VALUE" };
    (() => {
      using _ = disposableEnv({ OTHER_KEY: "OTHER_VALUE" });
      expect(process.env.KEY).toBe("VALUE");
      expect(process.env.OTHER_KEY).toBe("OTHER_VALUE");
    })();
    expect(process.env.KEY).toBe("VALUE");
    expect(process.env.OTHER_KEY).toBe(undefined);
  });

  it("can undo overwrites of the same variable", () => {
    process.env = { KEY: "VALUE1" };
    (() => {
      using _a = disposableEnv({ KEY: "VALUE2" });
      expect(process.env.KEY).toBe("VALUE2");
      using _b = disposableEnv({ KEY: "VALUE3" });
      expect(process.env.KEY).toBe("VALUE3");
      using _c = disposableEnv({ KEY: "VALUE4" });
      expect(process.env.KEY).toBe("VALUE4");
    })();
    expect(process.env.KEY).toBe("VALUE1");
  });

  it("deletes unused variable names after use", () => {
    process.env = { KEY1: "VALUE1" };
    expect(Object.keys(process.env)).toStrictEqual(["KEY1"]);
    (() => {
      using _ = disposableEnv({ KEY2: "VALUE2" });
      expect(Object.keys(process.env).sort()).toStrictEqual(["KEY1", "KEY2"]);
    })();
    expect(Object.keys(process.env)).toStrictEqual(["KEY1"]);
  });

  it("reverts altered variables that it sets", () => {
    process.env = { KEY: "VALUE1" };
    (() => {
      using _ = disposableEnv({ KEY: "VALUE2" });
      expect(process.env.KEY).toBe("VALUE2");
      process.env.KEY = "VALUE3";
      expect(process.env.KEY).toBe("VALUE3");
    })();
    expect(process.env.KEY).toBe("VALUE1");
  });

  it("does not revert variables that it does not set", () => {
    process.env = {};
    (() => {
      using _ = disposableEnv({ KEY1: "VALUE1" });
      process.env.KEY2 = "VALUE2";
    })();
    expect(process.env.KEY1).toBe(undefined);
    expect(process.env.KEY2).toBe("VALUE2");
  });

  it("does not clean up without using keyword", () => {
    process.env = {};
    (() => {
      // note: this may will hopefully either become invalid
      // typescript or a linting error in the future.
      disposableEnv({ KEY: "VALUE" });
    })();
    expect(process.env.KEY).toBe("VALUE");
  });
});
