# disposable-env

Disposable environment variable overrides.

## TypeScript 5.2

Using this correctly with NodeJS requires TypeScript 5.2 as well as a
polyfill (see example usage below). I wouldn't recommend it for
production code yet, since it's still in beta an a lot of important
tooling (prettier, eslint, etc) doesn't support it yet.

## Installation

1. Install TypeScript 5.2 (currently `yarn add -D typescript@beta` or
   `npm i -D typescript@beta`).
2. Install this package (`yarn add disposable-env` or `npm i disposable-env`).


## Usage

Here's an example of using it in a jest test.

```ts
import disposableEnv from "disposable-env";

// Either add this polyfill yourself, or use something like core-js
// See https://devblogs.microsoft.com/typescript/announcing-typescript-5-2-beta/
// @ts-expect-error
Symbol.dispose ??= Symbol("Symbol.dispose");

describe("myFunction", () => {
  it("connects using the SECRET_ACCESS_KEY environment variable", () => {
    using _ = disposableEnv({'SECRET_ACCESS_KEY':"sample access key"});
    expect(myFunction()).toBe("the mysteries of the universe");
  });

  // process.env is back to normal here
  it("throws an error when SECRET_ACCESS_KEY is unset", () => {
    expect(myFunction()).toThrow();
  });
});
```

## TODO

- ESM support
- Fix prettier (TypeScript 5.2 support in progress: https://github.com/prettier/prettier/issues/15004)
- Fix eslint (TypeScript 5.2 support in progress: https://github.com/typescript-eslint/typescript-eslint/issues/7155)