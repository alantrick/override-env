import * as process from "process";

/**
 * Set and automatically revert environment variables.
 *
 * This is intended to be used with the `using` keyword. For example,
 * you can use it to prevent environment bleeding in a Jest test:
 *
 * ```ts
 * test("there's no place like /dev/null", () => {
 *   using _ = disposableEnv({"HOME": "/dev/null"})
 *   ...
 * });
 *
 * test("HOME is no longer set here", () => { })
 * ```
 *
 * @param overrides key-value object of environment variable names -> values
 */
export default function disposableEnv(
  overrides: Record<string, string | undefined>
): Disposable {
  const oldEntries = Object.keys(overrides).map(
    (key) => [key, process.env[key]] as const
  );

  writeEnv(Object.entries(overrides));
  return {
    [Symbol.dispose]() {
      writeEnv(oldEntries);
    },
  };
}

const writeEnv = (entries: (readonly [string, string | undefined])[]) =>
  entries.forEach(([key, value]) => {
    if (value === undefined) delete process.env[key];
    else process.env[key] = value;
  });
